﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

    public static BoardManager mInstance;
    public GameObject spritePrefab;
    public List<Sprite> characters = new List<Sprite>();
    private GameObject[,] listTiles;
    public int xSize, ySize;
    public bool isShifting { get; set; }

	// Use this for initialization
	void Start () {
        mInstance = GetComponent<BoardManager>();
        Vector2 offset = spritePrefab.GetComponent<SpriteRenderer>().bounds.size;
        CreateBoard(offset.x, offset.y);
    }

    private void CreateBoard(float xOffset, float yOffset)
    {
        listTiles = new GameObject[xSize, ySize];

        float startX = transform.position.x;
        float startY = transform.position.y;

        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
            
                GameObject newTile = Instantiate(spritePrefab, new Vector3(startX + xOffset * x, startY + yOffset * y, 0), spritePrefab.transform.rotation);
                newTile.name = "tile_" + x + "_" + y;
                newTile.GetComponent<GameTile>().SetPos(x, y);
                listTiles[x,y] = newTile;
                Sprite newSprite = characters[Random.Range(0, characters.Count)];
                newTile.GetComponent<SpriteRenderer>().sprite = newSprite;
            }
        }
    }

    public IEnumerator  FindNullTile()
    {
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                if (listTiles[x, y].GetComponent<SpriteRenderer>().sprite == null)
                {
                    yield return StartCoroutine(ShiftTilesDown(x, y));
                    break;
                }
            }
        }
    }

    private IEnumerator ShiftTilesDown(int x, int yStart, float shiftDelay = 0.03f)
    {
        isShifting = true;
        List<SpriteRenderer> renders = new List<SpriteRenderer>();
        int nullCount = 0;

        for (int y = yStart; y < ySize; y++)
        {
            SpriteRenderer render = listTiles[x, y].GetComponent<SpriteRenderer>();
            if (render.sprite == null)
            {
                nullCount++;
            }
            renders.Add(render);
        }

        for (int i = 0; i < nullCount; i++)
        {
            yield return new WaitForSeconds(shiftDelay);
            for (int k = 0; k < renders.Count - 1; k++)
            {
                renders[k].sprite = renders[k + 1].sprite;
                renders[k + 1].sprite = GetNewSprite(x, ySize - 1);
            }
        }
        isShifting = false;
    }

    private Sprite GetNewSprite(int x, int y)
    {
        List<Sprite> possibleCharaters = new List<Sprite>();
        possibleCharaters.AddRange(characters);
        if (x > 0)
        {
            possibleCharaters.Remove(listTiles[x - 1, y].GetComponent<SpriteRenderer>().sprite);
        }
        if (x < xSize - 1)
        {
            possibleCharaters.Remove(listTiles[x + 1, y].GetComponent<SpriteRenderer>().sprite);
        }
        if (y > 0)
        {
            possibleCharaters.Remove(listTiles[x, y - 1].GetComponent<SpriteRenderer>().sprite);
        }

        return possibleCharaters[Random.Range(0, possibleCharaters.Count)];
    }
    // Update is called once per frame
    void Update () {
		
	}
}
