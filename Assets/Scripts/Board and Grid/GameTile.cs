﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTile : MonoBehaviour
{

    private static Color selectedColor = new Color(.5f, .5f, .5f, 1.0f);

    // lưu vị trí của gameTile đã chọn trc đó.
    private static GameTile m_previousSelected = null;
    private int xPos, yPos;
    private SpriteRenderer m_render;
    //private bool m_isSelected = false;
    private bool m_isMatchFound = false;
    private Vector2[] adjacentDirections = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };

    void Awake()
    {
        m_render = GetComponent<SpriteRenderer>();
    }

    private void Select()
    {
        //m_isSelected = true;
        m_render.color = selectedColor;

        //SFXManager.instance.PlaySFX(Clip.Select);
    }

    private void Deselect()
    {
        //m_isSelected = false;
        m_render.color = Color.white;
    }

    void OnMouseDown()
    {
        //Debug.Log(" - on mouse down ");
        if (m_render.sprite == null || BoardManager.mInstance.isShifting)
        {
            return;
        }

        Select();
        GameTile currGameTile = gameObject.GetComponent<GameTile>();

        if (m_previousSelected != null)
        {
            if (m_previousSelected != currGameTile)
            {
                m_previousSelected.Deselect();
                Select();
                // swap 2 tiles
                SwapGameTile(m_previousSelected.m_render);

                // test find match
                //FindMatch(adjacentDirections[0], adjacentDirections[1]);
                //FindMatch(adjacentDirections[2], adjacentDirections[3]);
                //m_previousSelected.FindMatch(adjacentDirections[0], adjacentDirections[1]);
                //m_previousSelected.FindMatch(adjacentDirections[2], adjacentDirections[3]);

                ClearAllMatch();
                m_previousSelected.ClearAllMatch();
                // sau khi swap, reset màu về bình thường.
                currGameTile.Deselect();
                StopCoroutine(BoardManager.mInstance.FindNullTile());
                StartCoroutine(BoardManager.mInstance.FindNullTile());
            }
            else
            {
                Deselect();
            }
            m_previousSelected = null;
        }
        else
        {
            m_previousSelected = currGameTile;
        }
    }

    void OnMouseUp()
    {
        //Debug.Log(" -- on mouse up ");
    }

    public void SwapGameTile(SpriteRenderer renderPreviousGameTile)
    {
        if (m_render.sprite == renderPreviousGameTile.sprite)
        {
            return;
        }

        // chỉ swap 2 gameTile nếu chúng nằm cạnh nhau.
        if (IsAdjacentTile())
        {
            Debug.Log("-- swap game tile ");
            Sprite tempSprite = renderPreviousGameTile.sprite;
            renderPreviousGameTile.sprite = m_render.sprite;
            m_render.sprite = tempSprite;

            //SFXManager.instance.PlaySFX(Clip.Swap);
        }
        else
        {
            Debug.Log("-- cannot swap these tiles ! ");
        }
    }

    private bool IsAdjacentTile()
    {
        // same column
        if (m_previousSelected.xPos == this.xPos)
        {
            if ((m_previousSelected.yPos == this.yPos + 1) || (m_previousSelected.yPos == this.yPos - 1))
            {
                return true;
            }
        }

        // same row
        if (m_previousSelected.yPos == this.yPos)
        {
            if ((m_previousSelected.xPos == this.xPos + 1) || (m_previousSelected.xPos == this.xPos - 1))
            {
                return true;
            }
        }

        return false;
    }

    /*
    * Hàm tìm kiếm match theo chiều horizontal, vertical
    * horizontal: dir1st, dir2nd = {Vec2.left, Vec2.right}
    * vertical: dir1st, dir2nd = {Vec2.up, Vec2.down}
    */
    private List<GameObject> FindMatch(Vector2 dir1st, Vector2 dir2nd)
    {
        List<GameObject> matchingTiles = new List<GameObject>();
        // check if orientation = horizontal.
        RaycastHit2D[] hit1st = Physics2D.RaycastAll(transform.position, dir1st);
        RaycastHit2D[] hit2nd = Physics2D.RaycastAll(transform.position, dir2nd);

        for (int i = 0; i < hit1st.Length; i++)
        {
            if (hit1st[i].collider && hit1st[i].collider.gameObject.GetComponent<SpriteRenderer>().sprite == m_render.sprite)
            {
                matchingTiles.Add(hit1st[i].collider.gameObject);
            }
            else
            {
                break;
            }
        }
        for (int i = 1; i < hit2nd.Length; i++)
        {
            if (hit2nd[i].collider && hit2nd[i].collider.gameObject.GetComponent<SpriteRenderer>().sprite == m_render.sprite)
            {
                matchingTiles.Add(hit2nd[i].collider.gameObject);
            }
            else
            {
                break;
            }
        }

        return matchingTiles;
    }

    private void ClearMatch(List<GameObject> matchingTiles)
    {
        if (matchingTiles.Count >= 3)
        {
            for (int i = 0; i < matchingTiles.Count; i++)
            {
                matchingTiles[i].GetComponent<SpriteRenderer>().sprite = null;
            }
            m_isMatchFound = true;
        }
    }

    public void ClearAllMatch()
    {
        if (m_render.sprite == null)
        {
            return;
        }

        ClearMatch(FindMatch(adjacentDirections[0], adjacentDirections[1]));
        ClearMatch(FindMatch(adjacentDirections[2], adjacentDirections[3]));
        if (m_isMatchFound)
        {
            m_render.sprite = null;
            m_isMatchFound = false;
            // play sound
        }
    }

    public void SetPos(int x, int y)
    {
        xPos = x;
        yPos = y;
    }
}
